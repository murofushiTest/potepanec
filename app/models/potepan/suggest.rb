class Potepan::Suggest < ApplicationRecord
  scope :find_by_keyword, -> (keyword) { where("keyword LIKE ?", "#{keyword}%") }

  def self.find_suggests(keyword, max_num)
    keywords = find_by_keyword(keyword)
    keywords = keywords.limit(max_num) if max_num =~ /^[1-9]$|^[1-9][0-9]+$/
    keywords
  end
end
