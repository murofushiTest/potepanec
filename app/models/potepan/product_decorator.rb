module Potepan::ProductDecorator
  def self.prepended(base)
    base.scope :include_master, -> { base.includes(master: [:default_price, :images]) }
  end

  def related_products(display_number)
    related_product_ids = products_with_same_taxons(display_number).keys
    Spree::Product.include_master.where(id: related_product_ids)
  end

  def products_with_same_taxons(display_number)
    Spree::Product.joins(:taxons).where(spree_taxons: { id: taxons }).where.not(id: id).\
      group('spree_products.id').order('count_spree_products_id desc').limit(display_number).count('spree_products.id')
  end

  Spree::Product.prepend self
end
