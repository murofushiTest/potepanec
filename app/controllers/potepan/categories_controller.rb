class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(taxons: :products)
    @taxon      = Spree::Taxon.find(params[:id])
    @products = if @taxon.depth == 0
                  @taxon.all_products.include_master
                else
                  @taxon.products.include_master
                end
  end
end
