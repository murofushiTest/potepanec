class Potepan::Api::SuggestsController < ApplicationController
  before_action :authenticate

  def index
    if params[:keyword].blank?
      render status: 400, json: "keyword can't be blank"
    else
      suggests = Potepan::Suggest.find_suggests(params[:keyword], params[:max_num]).pluck(:keyword)
      render json: suggests
    end
  end

  private

  def authenticate
    authenticate_or_request_with_http_token do |token, options|
      ActiveSupport::SecurityUtils.secure_compare(token, ENV['SUGGEST_API'])
    end
  end
end
