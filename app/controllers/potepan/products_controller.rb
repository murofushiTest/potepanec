class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @variant = @product.master
    @related_products = @product.related_products(POTEPAN::DISPLAY_NUMBER)
  end
end
