class Potepan::SuggestsController < ApplicationController
  def index
    url = ENV['POTEPAN_URL']
    client = HTTPClient.new
    query = { 'keyword' => params[:keyword], 'max_num' => POTEPAN::SUGGEST_MAX_NUM }
    header = { Authorization: "Bearer #{ENV['POTEPAN_API']}" }
    res = client.get(url, query, header)
    if res.status == 200
      render json: res.body
    else
      render status: 500, json: res.body
    end
  end
end
