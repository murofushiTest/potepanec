require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxon)   { create(:taxon, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get :show, params: { id: taxon.id }
  end

  it 'ステータスコードが200になるか' do
    expect(response.status).to eq(200)
  end

  it '@taxonを元にshow.html.erbへ遷移するか' do
    expect(response).to render_template :show
  end

  it '@taxonが期待された値を持っているか' do
    expect(assigns(:taxon)).to eq taxon
  end

  it '@taxonomiesが期待された値を持っているか' do
    expect(assigns(:taxonomies)).to match_array [taxonomy]
  end

  it '@productsが期待された値を持っているか' do
    expect(assigns(:products)).to match_array [product]
  end
end
