require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:product)   { create(:product, name: "GIANTS-BAT", taxons: [taxon]) }
  let(:taxon)      { create(:taxon, name: "BAT") }
  let!(:product_2) { create(:product, name: "SOFTBANK-BAT", taxons: [taxon]) }
  let!(:product_3) { create(:product, name: "DORAGONS-BAT", taxons: [taxon]) }

  before do
    get :show, params: { id: product.id }
  end

  it 'ステータスコードが200になるか' do
    expect(response.status).to eq(200)
  end

  it '@productを元にshow.html.erbへ遷移するか' do
    expect(response).to render_template :show
  end

  it '@productが期待された値を持っているか' do
    expect(assigns(:product)).to eq product
  end

  it '@variantが期待された値を持っているか' do
    expect(assigns(:variant)).to eq product.master
  end

  it '@related_productsが期待された値を持っているか' do
    expect(assigns(:related_products)).to eq [product_2, product_3]
  end
end
