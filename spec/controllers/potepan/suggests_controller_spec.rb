require 'rails_helper'
require 'webmock/rspec'

RSpec.describe Potepan::SuggestsController, type: :controller do
  before do
    WebMock.enable!
    WebMock.stub_request(:get, ENV['POTEPAN_URL']).with(
      headers: { 'Authorization' => "Bearer #{ENV['POTEPAN_API']}" },
      query: { 'keyword' => query_keyword, 'max_num' => 5 }
    ).to_return(
      body: return_body,
      status: return_status
    )
  end

  context 'webAPIから返ってくるステータスコードが200の場合' do
    let(:query_keyword) { 'm' }
    let(:return_body) { ["maeda", "maeda kenta", "maeken", "major", "MLB"] }
    let(:return_status) { 200 }

    before do
      get :index, params: { keyword: 'm', max_num: 5 }
    end

    it 'ステータスコードが200になる' do
      expect(response.status).to eq(200)
    end

    it '期待された値が返ってくる' do
      expect(JSON.parse(response.body)).to match_array ["maeda", "maeda kenta", "maeken", "major", "MLB"]
    end
  end

  context 'webAPIから返ってくるステータスコードが200以外の場合' do
    let(:query_keyword) { '' }
    let(:return_body) { 'error_message' }
    let(:return_status) { 500 }

    before do
      get :index, params: { keyword: '', max_num: 5 }
    end

    it 'keywordを空で渡すとステータスコード500を返す' do
      expect(response.status).to eq(500)
    end

    it 'エラーメッセージがbodyに入っている' do
      expect(response.body).to eq 'error_message'
    end
  end
end
