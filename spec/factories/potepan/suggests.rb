FactoryBot.define do
  factory :potepan_suggest, class: 'Potepan::Suggest' do
    keyword { "MyString" }
  end

  trait :initial_a do
    sequence(:keyword) { |n| "a#{n}" }
  end

  trait :initial_b do
    sequence(:keyword) { |n| "b#{n}" }
  end
end
