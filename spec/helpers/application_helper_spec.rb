require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe '#full_title' do
    it '引数がない場合、"BIGBAG Store"と返す' do
      expect(full_title).to eq "BIGBAG Store"
    end

    it '引数がある場合、"引数 - BIGBAG Store"と返す' do
      expect(full_title(page_title: "test")).to eq "test - BIGBAG Store"
    end

    it '引数がnilである場合、"BIGBAG Store"と返す' do
      expect(full_title(page_title: nil)).to eq "BIGBAG Store"
    end
  end
end
