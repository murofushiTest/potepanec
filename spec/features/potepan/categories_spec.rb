require 'rails_helper'

RSpec.feature 'categories', type: :feature do
  let(:taxonomy)         { create(:taxonomy) }
  let(:taxon)            { create(:taxon, name: "BAT", taxonomy: taxonomy) }
  let!(:product)         { create(:product, name: "GIANTS-BAT", taxons: [taxon]) }
  let(:another_taxon)    { create(:taxon, name: "cap", taxonomy: taxonomy) }
  let!(:another_product) { create(:product, name: "GIANTS-CAP", taxons: [another_taxon]) }

  before do
    product.master.images = [create(:image)]
  end

  scenario 'カテゴリーページ内の表示' do
    # 一つ目のカテゴリーページ
    visit potepan_category_path(taxon.id)
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    within ".productCaption" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).not_to have_content another_product.name
    end

    # 二つ目のカテゴリーページ
    click_on "#{another_taxon.name} (#{another_taxon.products.length})"
    expect(current_path).to eq potepan_category_path(another_taxon.id)
    expect(page).to have_title "#{another_taxon.name} - BIGBAG Store"
    within ".productCaption" do
      expect(page).to have_content another_product.name
      expect(page).to have_content another_product.display_price
      expect(page).not_to have_content product.name
    end

    # 商品詳細ページへ遷移
    click_on "#{taxon.name} (#{taxon.products.length})"
    click_on "products-img-#{product.id}"
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
