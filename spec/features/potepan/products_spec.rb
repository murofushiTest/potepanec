require 'rails_helper'

RSpec.feature 'products', type: :feature do
  let!(:taxons)     { create_list(:taxon, 3) }
  let!(:product)    { create(:product, taxons: [taxons[0], taxons[1]]) }
  let!(:products_1) { create_list(:product, 2, taxons: [taxons[0], taxons[2]]) }
  let!(:products_2) { create_list(:product, 3, taxons: [taxons[2]]) }

  scenario '商品詳細ページ内の表示' do
    visit potepan_product_path(products_1[0].id)
    expect(page).to have_title "#{products_1[0].name} - BIGBAG Store"

    # 詳細商品の表示
    within ".media-body" do
      expect(page).to have_content products_1[0].name
      expect(page).to have_content products_1[0].display_price
      expect(page).to have_content products_1[0].description
    end

    # 関連商品の表示
    first_related_products = [product, products_1[1], products_2[0], products_2[1]]
    first_related_products.each_with_index do |related_product, i|
      within ".productBox-#{i}" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end
    expect(page).not_to have_content products_2[2].name

    # 別の商品の関連商品の表示
    click_on product.name
    second_related_products = [products_1[0], products_1[1]]
    second_related_products.each_with_index do |related_product, i|
      within ".productBox-#{i}" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end
    expect(page).not_to have_content products_2[0].name

    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxons[0].id)
  end

  scenario 'ロゴ画像をクリックしトップページへ遷移' do
    visit potepan_product_path(product.id)
    click_on "title-logo"
    expect(current_path).to eq potepan_path
    expect(page).to have_title "BIGBAG Store"
  end
end
