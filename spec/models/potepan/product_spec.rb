require 'rails_helper'

RSpec.describe 'product_scope', type: :model do
  let!(:taxons)     { create_list(:taxon, 2) }
  let!(:products_1) { create_list(:product, 2, taxons: [taxons[0], taxons[1]]) }
  let!(:products_2) { create_list(:product, 3, taxons: [taxons[1]]) }

  describe 'products_with_same_taxons' do
    let(:products_order_5) { products_1[0].products_with_same_taxons(4).keys }
    let(:products_order_4) { products_2[0].products_with_same_taxons(4).keys }

    it 'taxonが同一の商品が関連商品の対象となる' do
      expect(products_order_4).to contain_exactly(products_1[0].id, products_1[1].id, products_2[1].id, products_2[2].id)
    end

    it '同一のtaxonが多い商品が優先的に出力される' do
      expect(products_order_5[0]).to eq products_1[1].id
    end

    it '対象の商品が５つ以上あるなら４つのみ出力される' do
      expect(products_order_5.length).to eq 4
    end

    it '対象の商品が４つなら４つ出力される' do
      expect(products_order_4.length).to eq 4
    end
  end

  describe 'related_products' do
    it '対象の商品が配列として出力される' do
      related_products = products_2[0].related_products(4)
      expect(related_products).to contain_exactly(products_1[0], products_1[1], products_2[1], products_2[2])
    end
  end
end
