require 'rails_helper'

RSpec.describe "Potepan::Api::Suggests", type: :request do
  let!(:suggests_a) { create_list(:potepan_suggest, 6, :initial_a) }
  let!(:suggest_b) { create(:potepan_suggest, :initial_b) }

  before do
    get potepan_api_suggests_path, params: params, headers: headers
  end

  context 'keywordに値を渡す場合' do
    let(:params) { { keyword: 'a', max_num: 3 } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it "渡した値で始まる単語が返ってくる" do
      expect(JSON.parse(response.body)).to match_array [suggests_a[0].keyword, suggests_a[1].keyword, suggests_a[2].keyword]
    end

    it "ステータスコードが200になる" do
      expect(response.status).to eq(200)
    end
  end

  context "keywordに値を渡さない場合" do
    let(:params) { { max_num: 5 } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it "ステータスコード400が返ってくる" do
      expect(response.status).to eq(400)
    end
  end

  context 'max_numに渡した数値よりもデータ数が多い場合' do
    let(:params) { { keyword: 'a', max_num: 5 } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it "データはmax_numに渡した数値分だけ返ってくる" do
      expect(JSON.parse(response.body).length).to eq 5
    end
  end

  context 'max_numに引数を渡さない場合' do
    let(:params) { { keyword: 'a' } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it 'データ数に上限なく返ってくる' do
      expect(JSON.parse(response.body).length).to eq 6
    end
  end

  context 'max_numに文字列を渡した場合' do
    let(:params) { { keyword: 'a', max_num: 'string' } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it 'データ数に上限なく返ってくる' do
      expect(JSON.parse(response.body).length).to eq 6
    end
  end

  context 'max_numにマイナスを渡した場合' do
    let(:params) { { keyword: 'a', max_num: -3 } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it 'データ数に上限なく返ってくる' do
      expect(JSON.parse(response.body).length).to eq 6
    end
  end

  context 'max_numに0を渡した場合' do
    let(:params) { { keyword: 'a', max_num: 0 } }
    let(:headers) { { Authorization: "Bearer #{ENV['SUGGEST_API']}" } }

    it 'データ数に上限なく返ってくる' do
      expect(JSON.parse(response.body).length).to eq 6
    end
  end

  context 'APIキーが間違っている場合' do
    let(:params) { { keyword: 'a', max_num: 3 } }
    let(:headers) { { Authorization: 'Bearer foobar' } }

    it 'ステータスコードが401になる' do
      expect(response.status).to eq(401)
    end
  end
end
